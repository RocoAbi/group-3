from firebase_admin import firestore

db = firestore.client()


def group_renderer(request):
    groups_ref = db.collections()
    group_ids = []
    for collection in groups_ref:
        group_ids.append(collection.id)

    context = dict([('all_group_ids', group_ids)])
    return context
