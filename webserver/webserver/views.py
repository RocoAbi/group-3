from django.shortcuts import render
import firebase_admin
import django_tables2
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.Certificate("firebase-adminsdk.json")
firebase_admin.initialize_app(cred, {
    'projectId': "fms-db-solution",
})

db = firestore.client()


def home(request):
    groups_ref = db.collections()
    group_ids = []
    for collection in groups_ref:
        group_ids.append(collection.id)

    context = dict([('group_ids', group_ids)])

    return render(request, "home/home.html", context)


def data_raw(request):
    return render(request, "home/data_raw.html")


def data_graph(request):
    return render(request, "home/data_graph.html")


def group(request, group):
    tables = []
    charts = []
    sensors = db.collection(group).stream()

    for sensor in sensors:
        readings = db.collection(group).document(sensor.id).collection('readings').stream()
        readings_list = []

        # For line chart
        test_dict = {}
        timestamps = []
        for reading in readings:
            timestamp = reading.id
            timestamps.append(timestamp)
            reading = reading.to_dict()
            reading_dict = {'timestamp': timestamp}

            class ReadingsTable(django_tables2.Table):
                timestamp = django_tables2.Column()
                for key in reading:
                    locals()[key] = django_tables2.Column()

                class Meta:
                    attrs = {"class": "raw-table"}

            for key in reading:
                if key not in test_dict:
                    num = [reading[key]]
                    test_dict[key] = num
                else:
                    test_dict[key].append(reading[key])
                reading_dict[key] = reading[key]

            readings_list.append(reading_dict)

        test_dict['timestamp'] = timestamps
        test_dict['sensor'] = sensor.id
        charts.append(test_dict)

        tables.append(ReadingsTable(readings_list))

    context = dict([('tables', tables)])
    context['charts'] = charts
    return render(request, "home/group.html", context)
