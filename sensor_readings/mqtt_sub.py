import json
import re
from datetime import datetime
import paho.mqtt.client as mqtt
import requests
from google.cloud.firestore import Client
from google.oauth2.credentials import Credentials
from requests.exceptions import HTTPError

brokerIP = "192.168.0.120"
brokerPort = 1883
brokerKeepAlive = 60
# TODO: Change topics (one topic for each specific sensor reading)
topics = [("esp/dht/temperature", 2), ("esp/dht/humidity", 2)]
data = {}

FIREBASE_REST_API = "https://identitytoolkit.googleapis.com/v1/accounts"


# Google verify password REST api to authenticate and generate user tokens
def sign_in_with_email_and_password(api_key, email, password):
    request_url = "%s:signInWithPassword?key=%s" % (FIREBASE_REST_API, api_key)
    headers = {"content-type": "application/json; charset=UTF-8"}
    data = json.dumps({"email": email, "password": password,
                      "returnSecureToken": True})

    req = requests.post(request_url, headers=headers, data=data)
    try:
        req.raise_for_status()
    except HTTPError as e:
        raise HTTPError(e, req.text)

    return req.json()


def on_connect(client, userdata, flags, rc):
    client.subscribe(topics)


def message(client, userdata, msg):
    topic = re.findall("[^\/]+$", str(msg.topic))

    if not topic[0] in data:
        data[topic[0]] = float(msg.payload.decode())

    if len(data) == len(topics):
        # TODO: Change group number
        # TODO: Change sensor name
        db.collection(u'group-3') \
            .document(u'dht11') \
            .collection(u'readings') \
            .document(str(datetime.now())) \
            .set(data)

        data.clear()


# TODO: Change email address and password
response = sign_in_with_email_and_password(
    "AIzaSyBFz4JhT9VDH6ZDOUAbyWWehvOvUCfHalI", "test@user.com", "test123")
# google.oauth2.credentials and the response object to create the correct user credentials
creds = Credentials(response['idToken'], response['refreshToken'])

# Use the raw firestore grpc client
db = Client("fms-db-solution", creds)

client = mqtt.Client()
client.connect(brokerIP, brokerPort, brokerKeepAlive)
client.on_connect = on_connect
client.on_message = message
client.loop_forever()
