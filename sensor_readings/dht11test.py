import requests
import json
import google.cloud.firestore
from requests.exceptions import HTTPError
from google.oauth2.credentials import Credentials
from google.cloud.firestore import Client
import time
from datetime import datetime
import board
import adafruit_dht

FIREBASE_REST_API = "https://identitytoolkit.googleapis.com/v1/accounts"


# Google verify password REST api to authenticate and generate user tokens
def sign_in_with_email_and_password(api_key, email, password):
    request_url = "%s:signInWithPassword?key=%s" % (FIREBASE_REST_API, api_key)
    headers = {"content-type": "application/json; charset=UTF-8"}
    data = json.dumps({"email": email, "password": password, "returnSecureToken": True})

    req = requests.post(request_url, headers=headers, data=data)
    try:
        req.raise_for_status()
    except HTTPError as e:
        raise HTTPError(e, req.text)

    return req.json()


response = sign_in_with_email_and_password("AIzaSyBFz4JhT9VDH6ZDOUAbyWWehvOvUCfHalI", "test@user.com", "test123")
# google.oauth2.credentials and the response object to create the correct user credentials
creds = Credentials(response['idToken'], response['refreshToken'])

# Use the raw firestore grpc client
db = Client("fms-db-solution", creds)

# Initialize sensors
dhtDevice = adafruit_dht.DHT11(board.D17)
while True:
    try:
        temperature_c = dhtDevice.temperature
        humidity = dhtDevice.humidity
        print("Temp: {:.1f} C    Humidity: {}% "
              .format(temperature_c, humidity))
        data = {
            u'temperature': temperature_c,
            u'humidity': humidity
        }
        doc_ref = db.collection(u'group-3') \
            .document(u'dht11') \
            .collection(u'readings') \
            .document(str(datetime.now())) \
            .set(data)

    except RuntimeError as error:
        print(error.args[0])
    time.sleep(10.0)
